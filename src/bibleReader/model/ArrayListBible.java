package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @author Anna Prins (provided the implementation)
 */
public class ArrayListBible implements Bible {

	// The Fields
	private ArrayList<Verse> verses;
	private String version;
	private String title;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param verses
	 *            All of the verses of this version of the Bible.
	 */
	public ArrayListBible(VerseList verses) {
		this.verses = verses.copyVerses();
		version = verses.getVersion();
		title = verses.getDescription();
	}

	@Override
	public int getNumberOfVerses() {
		return verses.size();
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public boolean isValid(Reference ref) {
		for(int i = 0; i < verses.size(); i++) {
			if(verses.get(i).getReference().equals(ref)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getVerseText(Reference r) {
		if(isValid(r)) {
			for(int i = 0; i < verses.size(); i++) {
				if(verses.get(i).getReference().equals(r)) {
					return verses.get(i).getText();
				}
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(Reference r) {
		for(int i = 0; i < verses.size(); i++) {
			if(verses.get(i).getReference().equals(r)) {
				return verses.get(i);
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference ref = new Reference(book, chapter, verse);
		for(int i = 0; i < verses.size(); i++) {
			if(verses.get(i).getReference().equals(ref)) {
				return verses.get(i);
			}
		}
		return null;
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for stage 4.
	// See the Bible interface for the documentation of these methods.
	// Do not over think these methods. All three should be pretty
	// straightforward to implement.
	// For Stage 8 (give or take a 1 or 2) you will re-implement them so they
	// work better.
	// At that stage you will create another class to facilitate searching and
	// use it here.
	// (Essentially these two methods will be delegate methods.)
	// ---------------------------------------------------------------------------------------------

	@Override
	public VerseList getAllVerses() {
		VerseList allVerses = new VerseList(version, title, verses);
		return allVerses;
	}

	@Override
	public VerseList getVersesContaining(String phrase) {
		if (phrase.equals("")) {
			return new VerseList(version, phrase);
		}
		
		String searchPhrase = phrase.toLowerCase();
		VerseList containing = new VerseList(version, phrase);
		String currentVerse = "";
		for(Verse verse : verses) {
			currentVerse = verse.getText().toLowerCase();
			if(currentVerse.contains(searchPhrase)) {
				containing.add(verse);
			}
		}
		return containing;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		if (phrase.equals("")) {
			return new ArrayList<Reference>(0);
		}
		
		String searchPhrase = phrase.toLowerCase();
		ArrayList<Reference> containing = new ArrayList<Reference>(0);
		
		String currentVerse = "";
		for(Verse verse : verses) {
			currentVerse = verse.getText().toLowerCase();
			if(currentVerse.contains(searchPhrase)) {
				containing.add(verse.getReference());
			}
		}
		return containing;
	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList containing = new VerseList(version, "Arbitrary list of Verses");
		
		for(Reference ref : references) {
			containing.add(getVerse(ref));
		}

		return containing;
	}
	
	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 7.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented
	// by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		// TODO Implement me: Stage 7
		return -1;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}
}
