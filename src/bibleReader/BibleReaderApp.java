package bibleReader;

import java.io.File;

import javax.swing.JFrame;
import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.*;

import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.TreeMapBible;
import bibleReader.model.VerseList;

/**
 * The main class for the Bible Reader Application.
 * 
 * @author cusack
 */
public class BibleReaderApp extends JFrame {
	// Change these to suit your needs.
	public static final int width = 600;
	public static final int height = 600;
	public String inputText;
	public JFrame frame;
	public JButton search;
	public JTextField input;
	public JPanel searchPanel;

	public static void main(String[] args) {
		new BibleReaderApp();
	}

	// Fields
	private BibleReaderModel model;
	private ResultView resultView;

	/**
	 * Default constructor. We may want to replace this with a different one.
	 */
	public BibleReaderApp() {

		model = new BibleReaderModel();
		File kjvFile = new File("kjv.atv");
		VerseList verses = BibleIO.readBible(kjvFile);

		Bible kjv = new ArrayListBible(verses);

		model.addBible(kjv);

		resultView = new ResultView(model);

		setupGUI();
		frame.pack();
		frame.setSize(width, height);

		// So the application exits when you click the "x".
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	/**
	 * Set up the main GUI. Make sure you don't forget to put resultView
	 * somewhere!
	 */
	private void setupGUI() {
		// The stage numbers below may change, so make sure to pay attention to
		// what the assignment says.
		// TODO Add passage lookup: Stage ?
		// TODO Add 2nd version on display: Stage ?
		// TODO Limit the displayed search results to 20 at a time: Stage ?
		// TODO Add 3rd versions on display: Stage ?
		// TODO Format results better: Stage ?
		// TODO Display cross references for third version: Stage ?
		// TODO Save/load search results: Stage ?
		// add an input JTextField

		// Create the frame
		frame = new JFrame("Bible Reader App");
		// create the components to be added to the container
		input = new JTextField(20);
		input.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				inputText = input.getText();
			}

			public void keyReleased(KeyEvent e) {

			}

			public void keyTyped(KeyEvent e) {

			}
		});

		// add a search button that searches based on the inputText
		search = new JButton("Search");
		search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultView.emptyResults();
				resultView.displayResults(inputText);
			}
		});

		// create the container to add components too
		Container contents = frame.getContentPane();
		contents.setLayout(new BorderLayout());
		// create a JPanel to add the search bar and button to
		searchPanel = new JPanel();
		searchPanel.setLayout(new FlowLayout());
		searchPanel.add(input);
		searchPanel.add(search);
		// add the components of the frame
		contents.add(searchPanel, BorderLayout.NORTH);
		contents.add(resultView, BorderLayout.CENTER);
	}

}
