package bibleReader;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.*;

import bibleReader.model.BibleReaderModel;
import bibleReader.model.Reference;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * The display panel for the Bible Reader.
 * 
 * @author cusack
 */
public class ResultView extends JPanel {

	public BibleReaderModel model;
	public JTextPane textArea;
	public JScrollPane scrollPane;
	public JEditorPane editorPane;
	public String searchResults;

	/**
	 * Construct a new ResultView and set its model to myModel. It needs to
	 * model to look things up.
	 * 
	 * @param myModel
	 *            The model this view will access to get information.
	 */
	public ResultView(BibleReaderModel myModel) {
		model = myModel;

		// create a JEditorPane
		editorPane = new JEditorPane();
		editorPane.setContentType("text/html");

		editorPane.setText("");

		// create a JScrollPane with the text area
		scrollPane = new JScrollPane(editorPane);
		scrollPane.setPreferredSize(new Dimension(500, 500));

		add(scrollPane, BorderLayout.CENTER);
		searchResults = "";

	}

	public void emptyResults() {
		searchResults = "";
	}

	/**
	 * 
	 * @param searchText
	 *            the text in the input field
	 */
	public void displayResults(String searchText) {
		ArrayList<Reference> references = model.getReferencesContaining(searchText);
		String[] versions = model.getVersions();
		System.out.println(Arrays.toString(versions));
		searchResults += "" + "<table>" + "<tr>" + "<th>Verse</th>";

		for (String version : versions) {
			searchResults += "<th>" + version + "</th>";
		}
		searchResults += "</tr>";

		// VerseList verses = model.getVerses(version, references);
		// for(Verse v: verses){
		//
		// }

		searchResults += "<tr>";

		for (Reference r : references) {
			searchResults += "<td>" + r.toString() + "</td>";

			for (String v : versions) {
				ArrayList<Reference> ref = new ArrayList<Reference>();
				ref.add(r);
				String text = "";
				VerseList verse = model.getVerses(v, ref);
				for (Verse ver : verse) {
					text = ver.getText();
				}
				searchResults += "<td>" + text + "</td>";

				ref = new ArrayList<Reference>();
			}

			searchResults += "</tr>";

		}

		searchResults += "</tr>";

		editorPane.setText(searchResults);
		editorPane.setCaretPosition(0);

	}

}
